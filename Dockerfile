FROM localhost:5000/python:3.9-slim
# Run your own local docker registry for better performance
# See https://docs.docker.com/registry/deploying/#run-a-local-registry
# docker run -d -p 5000:5000 --restart=always --name registry registry:2

ENV PIPENV_VENV_IN_PROJECT=1

WORKDIR /opt/
RUN pip install pipenv
COPY Pipfile.lock /opt/
RUN pipenv install --pre

COPY . /opt/

CMD [ "python", "hello.py" ]