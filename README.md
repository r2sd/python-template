# Python template project

1. Use VS Code.
   1. Install `Python` extension by Microsoft
2. Customise `.gitignore`
3. **Learn** and Install [pipenv \(`pipenv`\)](https://pipenv.readthedocs.io/en/latest/).
4. Add `export PIPENV_VENV_IN_PROJECT=1` to your `.bashrc` or `~/.bash_profile` or any shell configuration file so that `pipenv` knows that the virtual environments should be stored/used in the current directory.
5. Make sure `echo $PIPENV_VENV_IN_PROJECT` prints 1.
6. Run `pipenv install --pre --dev` to install all dependencies including dev dependencies and create `Pipfile.lock`.
   1. This is already done and the `Pipfile.lock` file is committed to the repo.
   2. From this point, running `pipenv install --pre --dev` will install dependencies from `Pipfile.lock`.
7. Instead, run `pipenv update --pre --dev` to update all dependencies including dev dependencies and update `Pipfile.lock`.
8. In VS Code settings, choose the Python interpreter in the current directory.
9. Restart VS Code.
10. For any dependency resolution issues, run `pipenv lock --pre --clear`.
11. To create a `requirements.txt` file without dev dependencies, run `pipenv lock -r > requirements.txt`
